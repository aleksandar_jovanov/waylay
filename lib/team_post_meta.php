<?php

add_filter( 'rwmb_meta_boxes', 'team_register_meta_boxes' );

/**
 * Register meta boxes
 *
 * @return void
 */
function team_register_meta_boxes( $meta_boxes )
{
	$prefix = 'team_';

	$meta_boxes[] = array(
		'id' => 'team_meta',
		'title' => __( 'Team fields', 'rwmb' ),
		'pages' => array( 'post', 'page' ),
		'context' => 'normal',
		'priority' => 'high',
		'autosave' => true,
		'fields' => array(
			// Job title
			array(
				'name'  => __( 'Job title ', 'rwmb' ),
				'id'    => "{$prefix}title",
				'desc'  => __( 'Job title (like: Co-Founder, etc.)', 'rwmb' ),
				'type'  => 'text',
				'std'   => __( '', 'rwmb' ),
				'clone' => false,
			),
                        // Linkedin profile link
			array(
				'name'  => __( 'Linkedin profile ', 'rwmb' ),
				'id'    => "{$prefix}linkedin",
				'desc'  => __( 'Link of Linkedin profile', 'rwmb' ),
				'type'  => 'text',
				'std'   => __( '', 'rwmb' ),
				'clone' => false,
			),
                        // Skype profile link
//			array(
//				'name'  => __( 'Skype profile ', 'rwmb' ),
//				'id'    => "{$prefix}skype",
//				'desc'  => __( 'Link of Skype profile', 'rwmb' ),
//				'type'  => 'text',
//				'std'   => __( '', 'rwmb' ),
//				'clone' => false,
//			)
                        // Twitter profile link
			array(
				'name'  => __( 'Twitter profile ', 'rwmb' ),
				'id'    => "{$prefix}twitter",
				'desc'  => __( 'Link of Twitter profile', 'rwmb' ),
				'type'  => 'text',
				'std'   => __( '', 'rwmb' ),
				'clone' => false,
			)
		)
	);
	return $meta_boxes;
}


