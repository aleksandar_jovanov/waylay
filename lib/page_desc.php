<?php

add_filter( 'rwmb_meta_boxes', 'page_desc_field' );

/**
 * Register meta boxes
 *
 * @return void
 */
function page_desc_field( $meta_boxes )
{
	$prefix = 'desc_';

	$meta_boxes[] = array(
		'id' => 'desc_meta',
		'title' => __( 'Description', 'rwmb' ),
		'pages' => array( 'post', 'page' ),
		'context' => 'normal',
		'priority' => 'high',
		'autosave' => true,
		'fields' => array(
			// Job title
			array(
				'name'  => __( 'Page/Post description ', 'rwmb' ),
				'id'    => "{$prefix}title",
				'desc'  => __( 'Page/Post description beneath title', 'rwmb' ),
				'type'  => 'text',
				'std'   => __( '', 'rwmb' ),
				'clone' => false,
			)
		)
	);
	return $meta_boxes;
}


