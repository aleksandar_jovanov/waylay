<?php
/**
 * Shortcodes
 */

// HR Line
function hr_line( $atts ) {
	extract( shortcode_atts(
		array(
			'line' => '1',
		), $atts )
	);
    return '<hr style="border-width:'.$line.'px" />';
}
add_shortcode( 'hr', 'hr_line' );

// Clearfix
function clearfix() {
    return '<div classs="clearfix"></div>';
}
add_shortcode( 'clearfix', 'clearfix' );

// column shortcodes
function column_func( $atts, $content = null ) {
    extract( shortcode_atts(
        array(
                'device' => '',
                'col' => '12',
                'class' => ''
                
        ), $atts )
    );
    return '<div class="col-'.$device.'-'.$col.' '.$class.'" >'.do_shortcode($content).'</div>';
}
add_shortcode( 'column', 'column_func' );

// row shortcodes
function row_func( $atts, $content = null ) {
    extract( shortcode_atts(
        array(
                'class' => ''
            
                
        ), $atts )
    );
    return '<div class="row '.$class.'">'.do_shortcode($content).'</div>';
}
add_shortcode( 'row', 'row_func' );
// div shortcodes
function div_func( $atts, $content = null ) {
    extract( shortcode_atts(
        array(
                'class' => ''
        ), $atts )
    );
    return '<div class="'.$class.'">'.do_shortcode($content).'</div>';
}
add_shortcode( 'div', 'div_func' );
// div with id shortcodes
function divid_func( $atts, $content = null ) {
    extract( shortcode_atts(
        array(
                'id' => '',
                'class' => ''
        ), $atts )
    );
    return '<div id='.$id.' class="'.$class.'">'.do_shortcode($content).'</div>';
}
add_shortcode( 'divid', 'divid_func' );
// container shortcodes
function container_func( $atts, $content = null ) {
    extract( shortcode_atts(
        array(
                'class' => ''
            
                
        ), $atts )
    );
    return '<div class="wrap container '.$class.'">'.do_shortcode($content).'</div>';
}
add_shortcode( 'container', 'container_func' );

// Quick quide - Step
function arrow_list_shortcode( $atts, $content=null ) {
    extract( shortcode_atts(
        array(
                'color' => 'blue',
                'headline' => ''
        ), $atts )
    );
    
    return '<h3 class="arrow-headline arrow-'.$color.'">'.$headline.'</h3>
            <ul class="checkmark">
            '.$content.'
            </ul>';
}
add_shortcode( 'arrow-list', 'arrow_list_shortcode' );

// Benefits
function benefits_list_shortcode( $atts, $content=null ) {
    extract( shortcode_atts(
        array(
                'color' => 'purple',
                'headline' => '',
                'class' => ''
        ), $atts )
    );
    
    return '<div class="benefits-wrapper benefits-'.$color.' '.$class.'"><h3 class="benefits-headline">'.$headline.'</h3>
            <ul class="checkmark">
            '.$content.'
            </ul></div>';
}
add_shortcode( 'benefits-list', 'benefits_list_shortcode' );

// Gray Box
function grey_box_shortcode( $atts, $content=null ) {
    extract( shortcode_atts(
        array(
                'side' => '',
                'img' => '',
                'width' => '',
                'class' => ''
        ), $atts )
    );
    
    return '<div class="gray-box '.$class.'">
    <div class="media">
      <a class="pull-'.$side.'" href="javascript:void(0);">
        <img class="media-object" width="'.$width.'" src="'.$img.'"/>
      </a>
      <div class="media-body">
       '.$content.'
      </div>
    </div></div>';
}
add_shortcode( 'gray-box', 'grey_box_shortcode' );

// Post map
function post_map_shortcode( $atts, $content=null ) {
    extract( shortcode_atts(
        array(
                'class' => ''
        ), $atts )
    );
    
    return '<div class="post-map '.$class.'"><p>'.$content.'</p></div>';
}
add_shortcode( 'post-map', 'post_map_shortcode' );