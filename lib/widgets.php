<?php
/**
 * Register sidebars and widgets
 */
function waylay_widgets_init() {
  // Sidebars
  register_sidebar(array(
    'name'          => __('Primary', 'waylay'),
    'id'            => 'sidebar-primary',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h4>',
    'after_title'   => '</h4>',
  ));
  
  register_sidebar(array(
    'name'          => __('Blog', 'waylay'),
    'id'            => 'sidebar-blog',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h4>',
    'after_title'   => '</h4>',
  ));
  
  register_sidebar(array(
    'name'          => __('Footer-right', 'waylay'),
    'id'            => 'footer-right',
    'before_widget' => '<div class="footer-right %1$s %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h4>',
    'after_title'   => '</h4>',
  ));


  register_sidebar(array(
    'name'          => __('Footer', 'waylay'),
    'id'            => 'sidebar-footer',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h4>',
    'after_title'   => '</h4>',
  ));

  // Widgets
  register_widget('waylay_Vcard_Widget');
}
add_action('widgets_init', 'waylay_widgets_init');

/**
 * Example vCard widget
 */
class waylay_Vcard_Widget extends WP_Widget {
  private $fields = array(
    'title'          => 'Title (optional)',
    'street_address' => 'Street Address',
    'locality'       => 'City/Locality',
    'region'         => 'State/Region',
    'postal_code'    => 'Zipcode/Postal Code',
    'tel'            => 'Telephone',
    'email'          => 'Email'
  );

  function __construct() {
    $widget_ops = array('classname' => 'widget_waylay_vcard', 'description' => __('Use this widget to add a vCard', 'waylay'));

    $this->WP_Widget('widget_waylay_vcard', __('waylay: vCard', 'waylay'), $widget_ops);
    $this->alt_option_name = 'widget_waylay_vcard';

    add_action('save_post', array(&$this, 'flush_widget_cache'));
    add_action('deleted_post', array(&$this, 'flush_widget_cache'));
    add_action('switch_theme', array(&$this, 'flush_widget_cache'));
  }

  function widget($args, $instance) {
    $cache = wp_cache_get('widget_waylay_vcard', 'widget');

    if (!is_array($cache)) {
      $cache = array();
    }

    if (!isset($args['widget_id'])) {
      $args['widget_id'] = null;
    }

    if (isset($cache[$args['widget_id']])) {
      echo $cache[$args['widget_id']];
      return;
    }

    ob_start();
    extract($args, EXTR_SKIP);

    $title = apply_filters('widget_title', empty($instance['title']) ? __('vCard', 'waylay') : $instance['title'], $instance, $this->id_base);

    foreach($this->fields as $name => $label) {
      if (!isset($instance[$name])) { $instance[$name] = ''; }
    }

    echo $before_widget;

    if ($title) {
      echo $before_title, $title, $after_title;
    }
  ?>
    <p class="vcard">
      <a class="fn org url" href="<?php echo home_url('/'); ?>"><?php bloginfo('name'); ?></a><br>
      <span class="adr">
        <span class="street-address"><?php echo $instance['street_address']; ?></span><br>
        <span class="locality"><?php echo $instance['locality']; ?></span>,
        <span class="region"><?php echo $instance['region']; ?></span>
        <span class="postal-code"><?php echo $instance['postal_code']; ?></span><br>
      </span>
      <span class="tel"><span class="value"><?php echo $instance['tel']; ?></span></span><br>
      <a class="email" href="mailto:<?php echo $instance['email']; ?>"><?php echo $instance['email']; ?></a>
    </p>
  <?php
    echo $after_widget;

    $cache[$args['widget_id']] = ob_get_flush();
    wp_cache_set('widget_waylay_vcard', $cache, 'widget');
  }

  function update($new_instance, $old_instance) {
    $instance = array_map('strip_tags', $new_instance);

    $this->flush_widget_cache();

    $alloptions = wp_cache_get('alloptions', 'options');

    if (isset($alloptions['widget_waylay_vcard'])) {
      delete_option('widget_waylay_vcard');
    }

    return $instance;
  }

  function flush_widget_cache() {
    wp_cache_delete('widget_waylay_vcard', 'widget');
  }

  function form($instance) {
    foreach($this->fields as $name => $label) {
      ${$name} = isset($instance[$name]) ? esc_attr($instance[$name]) : '';
    ?>
    <p>
      <label for="<?php echo esc_attr($this->get_field_id($name)); ?>"><?php _e("{$label}:", 'waylay'); ?></label>
      <input class="widefat" id="<?php echo esc_attr($this->get_field_id($name)); ?>" name="<?php echo esc_attr($this->get_field_name($name)); ?>" type="text" value="<?php echo ${$name}; ?>">
    </p>
    <?php
    }
  }
}
// Creating Latest blog posts
class blog_news_posts extends WP_Widget {

function __construct() {
parent::__construct(
// Base ID of your widget
'blog_news_posts', 

// Widget name will appear in UI
__('Latest Blog news', 'waylay'), 

// Widget description
array( 'description' => __( 'Latest Blog posts', 'waylay' ), ) 
);
}

// Creating widget front-end
// This is where the action happens
public function widget( $args, $instance ) {
$title = __( 'Latest blog news', 'waylay' );
$number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 10;
if ( ! $number )
        $number = 10;

// before and after widget arguments are defined by themes
echo $args['before_widget'];
if ( ! empty( $title ) )
echo '<div class="blog-news">'.$args['before_title'] . $title . $args['after_title'].'</div>';


// This is where you run the code and display the output
echo '<ul class="checkmark">';
    global $post;
    $blog = array('category' => 13,'posts_per_page'   => $number);
    $custom_posts = get_posts($blog);
    foreach($custom_posts as $post) : setup_postdata($post);
    echo '<li><a href="';
    echo the_permalink();
    echo '">';
    echo the_title();
    echo '</a>'; 
    echo '<br/>'; 
    echo '<span class="date">';
    echo get_the_time("d.m.Y");
    echo '</span>';
    echo '<span class="author">'.get_the_author().'</span></li>';
    endforeach;
echo $args['after_widget'];
}
		
// Widget Backend 
public function form( $instance ) {
$number    = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
// Widget admin form
?>
<p><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to show:' ); ?></label>
<input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" /></p>
<?php 
}
	
// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
$instance = array();
$instance['number'] = (int) $new_instance['number'];
return $instance;
}
} // Class blog_news_posts ends here

// Register and load the widget
function waylay_blog_widget() {
	register_widget( 'blog_news_posts' );
}
add_action( 'widgets_init', 'waylay_blog_widget' );

