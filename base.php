<?php get_template_part('templates/head'); ?>
<body <?php body_class(); ?>>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-50877508-1', 'waylay.io');
  ga('send', 'pageview');

</script>
  <!--[if lt IE 8]>
    <div class="alert alert-warning">
      <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'waylay'); ?>
    </div>
  <![endif]-->

  <?php
    // Header navigation
    do_action('get_header');
    get_template_part('templates/header-top-navbar');
    
    // Template header
    include waylay_template_path();
    
    ?>

</body>
</html>
