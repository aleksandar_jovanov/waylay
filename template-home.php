<?php
/*
Template Name: Home screen
*/
?>

<?php get_template_part('templates/header', 'home'); ?>
<?php get_template_part('templates/content', 'home'); ?>

<?php
    get_template_part('templates/feeds');
  
    // Template footer
    get_template_part('templates/footer-home3');
?>