<?php get_template_part('templates/header', 'page'); ?>
<div class="wrap container" role="document">
    <div class="content row">
        <main class="main <?php echo waylay_main_class(); ?>" role="main">
                  <div class="row">
                      <div class="col-sm-12 main-content">
                          <h1 class="page-title">Oops!</h1>
                          <h4 class="page-desc">It’s looking like you may have taken a wrong turn. Don’t worry... it happens to the best of us. </h4>
                          <h4 class="page-desc">Please, find what you looking for in navigation on top.</h4>
                      </div>
                  </div>
        </main><!-- /.main -->
    </div>
</div>
<?php
    get_template_part('templates/feeds');
  
    // Template footer
    get_template_part('templates/footer');
?>
