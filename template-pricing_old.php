<?php
/*
Template Name: Pricing Tables
*/
?>

<?php get_template_part('templates/header', 'pricing'); ?>
<?php get_template_part('templates/content', 'pricing'); ?>
<?php get_template_part('templates/register', 'modal'); ?>

<?php
/*    get_template_part('templates/feeds');*/
  
    // Template footer
    get_template_part('templates/footer');
?>