(function($) {

var waylay = {
  // All pages
  common: {
    init: function() {
        $('a[href*=#]:not([href=#])').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
              var target = $(this.hash);
              target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
              if (target.length) {
                $('html,body').animate({
                  scrollTop: target.offset().top - 70
                }, 1000);
                return false;
              }
            }
          });
          
          $('#notify-btn').click(function() {
              $("#registerModal").modal('hide');
          });
          $('#notify-btn2').click(function() {
              $("#trialModal").modal('hide');
          });
          
          $('.feature-page .feature-buttons a').click(function(){
              selectedPanel = $(this).data('id');
              $(this).parents('.row').find('.selected').removeClass('selected');
              $(this).addClass('selected');
              $('.feature-content').find('.current-tab').removeClass('current-tab');
              $('#' + selectedPanel).addClass('current-tab');
          });
          
          $('a.team-img-link').click(function() {
              $dataNmb = $(this).data('info');
              if(!$(this).hasClass('active')){
                  $(this).parents('.list-inline').find('.active').removeClass('active');
                  $(this).addClass('active');
              }
              $('.team-member-about').removeClass('activeInfo');
              $('#info-'+$dataNmb).addClass('activeInfo');
          });
          
          $('.carousel').carousel({
            interval: 8000
          });
          
          $(window).load(function(){
            $('.waylay-slider').flexslider({
              animation: "slide"
            });
          });
          
          $('#plan-terms').waypoint(function(direction) {
            var elem = $('#plans-scroll'),positionBtn = elem.offset().top;
            if (direction == 'down')
                elem.css({top:positionBtn + 'px',position:'absolute'});
            else
                elem.css({top:'auto',position:'fixed'});
          },{
            offset:450
          });
		  
    }
  },
  // Home page
  home: {
    init: function() {
        
    }
  },
  // About us page, note the change from about-us to about_us.
  about_us: {
    init: function() {
      // JavaScript to be fired on the about us page
    }
  }
};

// The routing fires all common scripts, followed by the page specific scripts.
// Add additional events for more control over timing e.g. a finalize event
var UTIL = {
  fire: function(func, funcname, args) {
    var namespace = waylay;
    funcname = (funcname === undefined) ? 'init' : funcname;
    if (func !== '' && namespace[func] && typeof namespace[func][funcname] === 'function') {
      namespace[func][funcname](args);
    }
  },
  loadEvents: function() {
    UTIL.fire('common');

    $.each(document.body.className.replace(/-/g, '_').split(/\s+/),function(i,classnm) {
      UTIL.fire(classnm);
    });
  }
};

$(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
