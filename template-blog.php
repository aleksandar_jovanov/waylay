<?php
/*
Template Name: Blog page
*/
?>

<?php get_template_part('templates/header', 'blog'); ?>
<?php get_template_part('templates/content', 'blog'); ?>

<?php
    get_template_part('templates/partners');
    // Template footer
    get_template_part('templates/footer');
?>
