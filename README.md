# [waylay Theme](http://waylay.io/)

[![Built with Grunt](https://cdn.gruntjs.com/builtwith.png)](http://gruntjs.com/)

waylay is a WordPress starter theme based on [HTML5 Boilerplate](http://html5boilerplate.com/) & [Bootstrap](http://getbootstrap.com/) that will help you make better themes.

* Source: [https://github.com/waylay/waylay](https://github.com/waylay/waylay)
* Home Page: [http://waylay.io/](http://waylay.io/)
* Twitter: [@retlehs](https://twitter.com/retlehs)
* Newsletter: [Subscribe](http://waylay.io/subscribe/)
* Forum: [http://discourse.waylay.io/](http://discourse.waylay.io/)

## Installation

Clone the git repo - `git clone git://github.com/waylay/waylay.git` - or [download it](https://github.com/waylay/waylay/zipball/master) and then rename the directory to the name of your theme or website. [Install Grunt](http://gruntjs.com/getting-started), and then install the dependencies for waylay contained in `package.json` by running the following from the waylay theme directory:

```
npm install
```

Reference the [theme activation](http://waylay.io/waylay-101/#theme-activation) documentation to understand everything that happens once you activate waylay.

## Theme Development

After you've installed Grunt and ran `npm install` from the theme root, use `grunt watch` to watch for updates to your LESS and JS files and Grunt will automatically re-build as you write your code.

## Configuration

Edit `lib/config.php` to enable or disable support for various theme functions and to define constants that are used throughout the theme.

Edit `lib/init.php` to setup custom navigation menus and post thumbnail sizes.

## Documentation

### [waylay Docs](http://waylay.io/docs/)

* [waylay 101](http://waylay.io/waylay-101/) — A guide to installing waylay, the files and theme organization
* [Theme Wrapper](http://waylay.io/an-introduction-to-the-waylay-theme-wrapper/) — Learn all about the theme wrapper
* [Build Script](http://waylay.io/using-grunt-for-wordpress-theme-development/) — A look into the waylay build script powered by Grunt
* [waylay Sidebar](http://waylay.io/the-waylay-sidebar/) — Understand how to display or hide the sidebar in waylay

## Features

* Organized file and template structure
* HTML5 Boilerplate's markup along with ARIA roles and microformat
* Bootstrap
* [Grunt build script](http://waylay.io/using-grunt-for-wordpress-theme-development/)
* [Theme activation](http://waylay.io/waylay-101/#theme-activation)
* [Theme wrapper](http://waylay.io/an-introduction-to-the-waylay-theme-wrapper/)
* Root relative URLs
* Cleaner HTML output of navigation menus
* Cleaner output of `wp_head` and enqueued scripts/styles
* Nice search (`/search/query/`)
* Image captions use `<figure>` and `<figcaption>`
* Example vCard widget
* Posts use the [hNews](http://microformats.org/wiki/hnews) microformat
* [Multilingual ready](http://waylay.io/wpml/) (Brazilian Portuguese, Bulgarian, Catalan, Danish, Dutch, English, Finnish, French, German, Hungarian, Indonesian, Italian, Korean, Macedonian, Norwegian, Polish, Russian, Simplified Chinese, Spanish, Swedish, Traditional Chinese, Turkish, Vietnamese, Serbian)

## Contributing

Everyone is welcome to help [contribute](CONTRIBUTING.md) and improve this project. There are several ways you can contribute:

* Reporting issues (please read [issue guidelines](https://github.com/necolas/issue-guidelines))
* Suggesting new features
* Writing or refactoring code
* Fixing [issues](https://github.com/waylay/waylay/issues)
* Replying to questions on the [forum](http://discourse.waylay.io/)

## Support

Use the [waylay Discourse](http://discourse.waylay.io/) to ask questions and get support.
