<div class="jumbotron">
    <div class="container">
        <div class="row text-right">
            <div class="bg">
                <img alt="Waylay" class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/home-header-2.png"/>
            </div>
            <div class="txt-box">
                <h1 class="headline">SMART REASONING FOR IoT</h1>
                <h2>Turning data into actionable decisions</h2>
                <button type="submit" class="btn btn-danger" data-toggle="modal" data-target="#registerModal">Request a Demo!</button>
            </div>
        </div>
    </div>
</div>
