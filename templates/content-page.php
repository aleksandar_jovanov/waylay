<div class="wrap container" role="document">
    <div class="content row">
        <main class="main <?php echo waylay_main_class(); ?>" role="main">
                  <div class="row">
                      <div class="col-sm-12 main-content">
                          <h1 class="page-title"><?php echo waylay_title(); ?></h1>
                          <h4 class="page-desc"><?php $desc = get_post_meta( get_the_ID(), 'desc_title', true ); echo $desc; ?></h4>
                          <?php while (have_posts()) : the_post(); ?>
                            <?php the_content(); ?>
                            <?php wp_link_pages(array('before' => '<nav class="pagination">', 'after' => '</nav>')); ?>
                          <?php endwhile; ?>
                      </div>
                  </div>
        </main><!-- /.main -->
    </div>
</div>