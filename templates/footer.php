<footer class="footer footer-dark">
  <div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-3">
                    <div class="feed-box contact-box">
                        <ul class="address-list">
                            <li class="home">waylay BVBA<br/>Gaston Crommenlaan 8<br/>9050 Ghent<br/>Belgium</li>
                            <li class="vat">Inter. VAT:<br/>BE 0544.746.258</li>
                            <li class="phone">+32 493 257 438</li> 
                           <li class="mail"><a href="mailto:info@waylay.io">info@waylay.io</a></li>
                            <!--  <li class="mail">info@waylay.io</li> -->
                        </ul>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="feed-box blog-news">
                        <h4>Blog news</h4>
                        <div class="blog-feeds">
                        <?php $catname = 'blog';
                            $posts = get_posts("category_name=$catname&numberposts=2&offset=0");
                            foreach ($posts as $post) : start_wp();
                        ?>
                        <div class="media">
                            <div class="pull-left red-box">
                                <span class="month"><?php echo get_the_time('M'); ?></span>
                                <span class="date"><?php echo get_the_time('d'); ?></span>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading"><?php the_title(); ?></h4>
                                <?php the_excerpt(); ?>
                              </div>
                        </div>
                        <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="feed-box twitter-feed">
                          <?php dynamic_sidebar('Footer-right'); ?>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
  </div>
</footer>
<div class="footer-bottom">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="row">
<!--                    <div class="col-sm-3 text-center">
                        <a class="footer-logo" href="<?php echo home_url(); ?>/"><img alt="<?php bloginfo('name'); ?>" src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-small.png"/></a>
                    </div>-->
                    <div class="col-sm-10">
                    <?php
                      if (has_nav_menu('footer')) :
                        wp_nav_menu(array('theme_location' => 'footer', 'menu_class' => 'list-inline footer-menu'));
                      endif;
                      ?>
                    </div>
                    <div class="col-sm-2">
                        <p class="text-center copyright">&copy; <?php bloginfo('name'); ?> 2014.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php wp_footer(); ?>
