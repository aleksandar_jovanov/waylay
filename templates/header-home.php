<div class="jumbotron">
    <div class="container">
        <div class="row">
            <h1 class="headline text-center">Brings smart reasoning to the <br/><strong>Internet-of-Things</strong></h1>
            <img alt="Waylay" class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/home-header.png"/>
        </div>
    </div>
</div>
