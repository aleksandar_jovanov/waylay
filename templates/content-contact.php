<div class="wrap container" role="document">
    <div class="content row">
        <main class="main <?php echo waylay_main_class(); ?>" role="main">
                  <div class="row">
                      <div class="col-sm-12 main-content">
                          <h1 class="page-title"><?php echo waylay_title(); ?></h1>
                          <h4 class="page-desc">We want to hear from you!</h4>
                          <div class="contact-wrap">
                            <div class="col-md-8">
                                <h4 class="title">Contact</h4>
                                <div class="row">
                                    <div class="col-sm-5">
                                        <ul class="address-list">
                                            <li class="home">waylay BVBA<br/>Gaston Crommenlaan 8<br/>9050 Ghent<br/>Belgium<br/><br/>international VAT:<br/>
BE 0544.746.258</li>
                                            <li class="phone">+32 493 257 438</li> 
                                            <li class="mail">Email: info@waylay.io</li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-7">
                                        <?php echo do_shortcode('[wpgmza id="1"]'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <h4 class="title">Send us a message</h4>
                                <div class="contact-form-wrap">
                                    <?php echo do_shortcode('[contact-form-7 id="4204" title="Send us a message"]'); ?>
                                </div>
                            </div>
                            <?php while (have_posts()) : the_post(); ?>
                              <?php the_content(); ?>
                              <?php wp_link_pages(array('before' => '<nav class="pagination">', 'after' => '</nav>')); ?>
                            <?php endwhile; ?>
                          </div>
                      </div>
                  </div>
        </main><!-- /.main -->
    </div>
</div>