<div class="partners">
    <div class="container">
        <h3 class="thin-txt">Our customers & partners</h3>
        <ul class="list-inline partners-list">
            <li style="vertical-align: 6px;"><a href="http://www.eitictlabs.eu/" target="_blank"><img src="http://www.waylay.io/wp-content/uploads/2015/02/eit.png" class="img-responsive cl"/><img src="http://www.waylay.io/wp-content/uploads/2015/02/eit-bw.png" class="img-responsive bw"/></a></li>
            <li><a href="http://www.virdata.com/" target="_blank"><img src="http://www.waylay.io/wp-content/uploads/2014/11/virdata.png" class="img-responsive cl"/><img src="http://www.waylay.io/wp-content/uploads/2014/11/virdata-bw.png" class="img-responsive bw"/></a></li>
            <li><a href="http://www.cloudcake.be/" target="_blank"><img src="http://www.waylay.io/wp-content/uploads/2014/11/cloudcake.png" class="img-responsive cl"/><img src="http://www.waylay.io/wp-content/uploads/2014/11/cloudcake-bw.png" class="img-responsive bw"/></a></li>
            <li><a href="http://www.iminds.be" target="_blank"><img src="http://www.waylay.io/wp-content/uploads/2014/11/iminds.png" class="img-responsive cl"/><img src="http://www.waylay.io/wp-content/uploads/2014/11/iminds-bw.png" class="img-responsive bw"/></a></li>
            <li class="clearfix hidden-sm"></li>
            <li><a href="http://www.sensolus.com/" target="_blank"><img src="http://www.waylay.io/wp-content/uploads/2014/11/sensolus.png" class="img-responsive cl"/><img src="http://www.waylay.io/wp-content/uploads/2014/11/sensolus-bw.png" class="img-responsive bw"/></a></li>
            <li><a href="http://www.studiodott.be/nl/studiodott/" target="_blank"><img src="http://www.waylay.io/wp-content/uploads/2014/11/studiodott.png" class="img-responsive cl"/><img src="http://www.waylay.io/wp-content/uploads/2014/11/studiodott-bw.png" class="img-responsive bw"/></a></li>
        </ul>
    </div>
</div>