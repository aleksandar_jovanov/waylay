<div class="wrap container" role="document">
    <div class="content row">
        <main class="main <?php echo waylay_main_class(); ?>" role="main">
                  <div class="row">
                      <div class="col-sm-12 main-content">
                          <h1 class="page-title"><?php $category = get_the_category(); echo $category[0]->cat_name; ?></h1>
                          <h4 class="page-desc"><?php $desc = get_post_meta( get_the_ID(), 'desc_title', true ); echo $desc; ?></h4>
                          <div class="row">
                              <div class="col-sm-8">
                                  <div class="post-wrap">
                                    <?php while (have_posts()) : the_post(); ?>
                                      <!--Blog post start-->
                                        <div class="blog-post">
                                                <div class="date-box">
                                                    <div class="red-box">
                                                        <span class="month"><?php echo get_the_time('M'); ?></span>
                                                        <span class="date"><?php echo get_the_time('d'); ?></span>
                                                    </div>
                                                </div>
                                                <div class="article">
                                                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                                    <?php if (has_post_thumbnail( $post->ID ) ):
                                                        the_post_thumbnail('blog-thumb');
                                                     endif; ?>
                                                    <?php get_template_part('templates/entry-meta'); ?>
                                                    <div class="post-content">
                                                        <?php the_content(); ?>
                                                    </div>    
                                                </div>    
                                                <div class="clearfix"></div>
                                        </div>
                                        <!--Blog post end-->
                                      <?php endwhile; ?>
                                      <?php comments_template('/templates/comments.php');?>
                                  </div>
                              </div>
                              <div class="col-sm-4">
                                <?php if (waylay_display_sidebar()) : ?>
                                  <div class="sidebar">
                                    <?php dynamic_sidebar('Blog'); ?>
                                <?php endif; ?>
                              </div>
                          </div>
                      </div>
                  </div>
            </div>
        </main><!-- /.main -->
    </div>
</div>