<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Sign up for demo!</h4>
      </div>
      <div class="modal-body">
           <form action="http://waylay.us8.list-manage.com/subscribe/post?u=c51ba39f3fb039e291480d0b8&amp;id=197048b176" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" target="_blank">
                <div class="form-group">
                        <label class="sr-only" for="MERGE1">First Name</label>
                        <input class="form-control" type="text" name="MERGE1" id="MERGE1" placeholder="First Name">
                </div>
                <div class="form-group">
                        <label class="sr-only" for="MERGE2">Last Name</label>
                        <input class="form-control" type="text" name="MERGE2" id="MERGE2" placeholder="Last Name">
                </div>
                <div class="form-group">
                        <label class="sr-only" for="MERGE0">Business Email</label>
                        <input class="form-control " type="email" name="MERGE0" id="MERGE0" placeholder="Business Email">
                </div>
                <div class="form-group">
                        <label class="sr-only" for="MERGE4">Company</label>
                        <input class="form-control" type="text" name="MERGE4" id="MERGE4" placeholder="Company">
                </div>
                <div class="form-group">
                        <label class="sr-only" for="MERGE3">Phone Number</label>
                        <input class="form-control" type="text" name="MERGE3" id="MERGE3" placeholder="Phone Number">
                </div>
                <div class="form-group">
                    <div class="text-right">
                        <p class="help-block text-right"><small><i>All fields are required</i></small></p>
                        <button id="notify-btn" type="submit" class="btn btn-danger">Submit <i class="fa fa-sign-out fa-lg"></i></button> 
                    </div>
                </div>
                <div style="position: absolute; left: -5000px;"><input type="text" name="b_c51ba39f3fb039e291480d0b8_197048b176" tabindex="-1"/></div>
            </form>
            </div>
      </div>
    </div>
  </div>
</div>
<!--<div class="modal fade" id="trialModal" tabindex="-1" role="dialog" aria-labelledby="trialModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Free Trial</h4>
      </div>
      <div class="modal-body">
           <form action="http://waylay.us8.list-manage1.com/subscribe/post?u=c51ba39f3fb039e291480d0b8&amp;id=697094d319" method="post" id="mc-embedded-subscribe-form2" name="mc-embedded-subscribe-form2" target="_blank">
                <div class="form-group">
                    <p>In just a few steps, you can complete the form for a <strong>30 days free trial</strong> of waylay.</p>
                    <p>We will contact you shortly, to understand your needs, such that you can make most of the trial!</p>
                </div>
                <div class="form-group">
                        <label class="sr-only" for="MERGE1">First Name</label>
                        <input class="form-control" type="text" name="MERGE1" id="MERGE1" placeholder="First Name">
                </div>
                <div class="form-group">
                        <label class="sr-only" for="MERGE2">Last Name</label>
                        <input class="form-control" type="text" name="MERGE2" id="MERGE2" placeholder="Last Name">
                </div>
                <div class="form-group">
                        <label class="sr-only" for="MERGE0">Business Email</label>
                        <input class="form-control " type="email" name="MERGE0" id="MERGE0" placeholder="Business Email">
                </div>
                <div class="form-group">
                        <label class="sr-only" for="MERGE5">Company</label>
                        <input class="form-control" type="text" name="MERGE5" id="MERGE5" placeholder="Company">
                </div>
                <div class="form-group">
                        <label class="sr-only" for="MERGE4">Phone Number</label>
                        <input class="form-control" type="text" name="MERGE4" id="MERGE4" placeholder="Phone Number">
                </div>
                <div class="form-group">
                     <ul class="list-inline pull-left links">
                        <li class="menu-terms-of-use"><small><a href="/tou/" target="_blank">Terms Of Use</a></small></li>
                        <li class="menu-privacy-policy"><small><a href="/privacy/" target="_blank">Privacy Policy</a></small></li>
                    </ul>
                    <div class="text-right">
                        <p class="help-block"><small><i>All fields are required</i></small></p>
                        <button id="notify-btn2" type="submit" class="btn btn-danger">Submit <i class="fa fa-sign-out fa-lg"></i></button> 
                    </div>
                </div>
                <div style="position: absolute; left: -5000px;"><input type="text" name="b_c51ba39f3fb039e291480d0b8_697094d319" tabindex="-1" value=""></div>
            </form>
            </div>
      </div>
    </div>
  </div>-->
<div class="modal fade" id="trialModal" tabindex="-1" role="dialog" aria-labelledby="trialModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Free Trial</h4>
      </div>
        <div class="modal-body">
           <p>In just a few steps, you can complete the form for a <strong>30 days free trial</strong> of waylay.</p>
           <p>We will contact you shortly, to understand your needs, such that you can make most of the trial!</p>
           <?php echo do_shortcode( '[contact-form-7 id="5317" title="Send us message test"]' ) ?>
        </div>
      </div>
    </div>
  </div>