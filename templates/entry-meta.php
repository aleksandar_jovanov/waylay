<?php
$category = get_the_category();
$num_comments = get_comments_number();

if ( $num_comments == 0 ) {
        $comments = __('No Comments');
} elseif ( $num_comments > 1 ) {
        $comments = $num_comments . __(' Comments');
} else {
        $comments = __('1 Comment');
}
?>

<div class="entry-meta">
    <ul class="list-inline">
        <li class="author"><?php echo __('By', 'waylay'); ?> <a href="http://www.waylay.io/founders/" rel="author"><?php echo get_the_author(); ?></a></li>
        <li class="category"><?php echo $category[0]->cat_name;?></li>
        <li class="comments"><?php echo $comments;?></li>
    </ul>
</div>
