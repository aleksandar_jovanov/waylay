<footer class="footer">
  <div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="row">
                <p class="text-center copyright">Copyright &copy; <?php bloginfo('name'); ?> 2014.</p>
            </div>
        </div>
    </div>
  </div>
</footer>

<?php wp_footer(); ?>
