<section class="slider">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 column">
                <h3>Connect – Reason - Act</h3>
                <p>Waylay is the rules engine for smart connected device solutions. Our PaaS platform connects devices, applications and online services to enable better real-time decisions and actions. Waylay notifies, automates, predicts and diagnoses, all from one platform. Developers will love the modularity and flexibility to tune data sources, logic and output channels to their specific needs.</p>
            </div>
            <div class="col-sm-8 column">
                <div class="waylay-slider">
                    <ul class="slides">
                      <li>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/slide1.jpg" />
                      </li>
                      <li>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/slide2.jpg" />
                      </li>
                      <li>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/slide3.jpg" />
                      </li>
                      <li>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/slide4.jpg" />
                      </li>
                      <li>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/slide5.jpg" />
                      </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="feature-top">
    <div class="container">
        <img alt="Waylay feature graph" class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/graph.png"/>
    </div>
</div>
<main class="feature-page">
    <?php while (have_posts()) : the_post(); ?>
    <?php the_content(); ?>
    <?php endwhile; ?>
</main><!-- /.main -->