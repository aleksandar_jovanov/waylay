<div class="feature-top">
    <div class="container">
        <img alt="Waylay feature graph" class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/graph.png"/>
    </div>
</div>
<main class="feature-page feature-page2">
    <div class="wrap container">
        <div class="content row">
        <?php while (have_posts()) : the_post(); ?>
        <?php the_content(); ?>
        <?php endwhile; ?>
        </div>
    </div>
</main><!-- /.main -->