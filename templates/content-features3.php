<section class="slider">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 column">
                <h3>Connect – Reason - Act</h3>
                <br/>
                <p>Waylay is the rules engine for smart connected device solutions. Our PaaS platform connects devices, applications and online services to enable better real-time decisions and actions. Waylay notifies, automates, predicts and diagnoses, all from one platform. Developers will love the modularity and flexibility to tune data sources, logic and output channels to their specific needs.</p>
                <a href="<?php echo get_template_directory_uri(); ?>/assets/pdf/brochure_waylay.pdf" target="_blank" class="btn btn-transparent">Download this pdf to learn more...</a></p>
            </div>
            <div class="col-sm-8 column">
                <div class="waylay-slider">
                    <ul class="slides">
                      <li>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/slider/11.jpeg" />
                      </li>
                      <li>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/slider/21.jpeg" />
                      </li>
                      <li>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/slider/31.jpeg" />
                      </li>
                      <li>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/slider/41.jpeg" />
                      </li>
                      <li>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/slider/51.jpeg" />
                      </li>
                      <li>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/slider/61.jpeg" />
                      </li>
                      <li>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/slider/71.jpeg" />
                      </li>
                      <li>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/slider/81.jpeg" />
                      </li>
                      <li>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/slider/91.jpeg" />
                      </li>
                      <li>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/slider/101.jpeg" />
                      </li>
                      <li>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/slider/111.jpeg" />
                      </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<br>
<div class="feature-top">
    <div class="container">
        <img alt="Waylay feature graph" class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/graph.png"/>
    </div>
</div>
<main class="feature-page feature-page2">
    <div class="wrap container">
        <div class="content row">
        <?php while (have_posts()) : the_post(); ?>
        <?php the_content(); ?>
        <?php endwhile; ?>
        </div>
    </div>
</main><!-- /.main -->