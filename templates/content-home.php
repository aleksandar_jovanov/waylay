<div class="wrap container" role="document">
    <div class="content row">
      <main class="main <?php echo waylay_main_class(); ?>" role="main">
                <div class="row">
                    <div class="col-sm-12 main-content">
                      <?php while (have_posts()) : the_post(); ?>
                        <?php remove_filter( 'the_content', 'wpautop' ); the_content(); ?>
                      <?php endwhile; ?>
                    </div>
                </div>
      </main><!-- /.main -->
      <?php if (waylay_display_sidebar()) : ?>
        <aside class="sidebar <?php echo waylay_sidebar_class(); ?>" role="complementary">
          <?php include waylay_sidebar_path(); ?>
        </aside><!-- /.sidebar -->
      <?php endif; ?>
    </div><!-- /.content -->
</div><!-- /.wrap -->
<div class="contact-section">
    <div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="row centered">
<!-- 
                <form  action="http://feedburner.google.com/fb/a/mailverify" method="post" target="popupwindow" onsubmit="window.open('http://feedburner.google.com/fb/a/mailverify?uri=waylayio', 'popupwindow', 'scrollbars=yes,width=550,height=520');return true">
 -->
 <form action="http://waylay.us8.list-manage.com/subscribe/post?u=c51ba39f3fb039e291480d0b8&amp;id=197048b176" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>

                    <div class="form-group">
                        <div class="col-sm-3 col-sm-offset-3">
                            <label class="sr-only" for="notify-email">Enter your email...</label>
                            <!-- <input type="text" name="email" class="form-control" id="notify-email" placeholder="Enter your email..."> -->
<input type="email" value="" name="EMAIL" id="mce-EMAIL" class="form-control" placeholder="Enter your email...">  
                 
                        </div>
                        <div class="col-sm-3">
                        <button id="notify-btn" type="submit" class="btn btn-danger">Request a Demo!</button>   
<!-- 
                        <input type="hidden" value="waylayio" name="uri"/><input type="hidden" name="loc" value="en_US"/>
                            <button id="notify-btn" type="submit" class="btn btn-danger">Request a Demo!</button>
                            
 -->
                    	</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;"><input type="text" name="b_c51ba39f3fb039e291480d0b8_197048b176" tabindex="-1" value=""></div>


<!--         <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="btn btn-danger">  -->                      
                            
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
    </div>
</div>




