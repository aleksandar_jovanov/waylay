<footer class="footer">
  <div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="row">
                <div class="grey-box">
                    <form action="http://waylay.us8.list-manage.com/subscribe/post?u=c51ba39f3fb039e291480d0b8&amp;id=6b334f45b6" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate">
                        <div class="form-group">
                            <div class="col-sm-9">
                                <label class="sr-only" for="notify-email">Email address</label>
                                <input type="email" value="" name="EMAIL" class="form-control" id="mce-EMAIL" placeholder="Enter your email to request a demo ...">
                            </div>
                             <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;"><input type="text" name="b_c51ba39f3fb039e291480d0b8_6b334f45b6" tabindex="-1" value=""></div>
                            <div class="col-sm-3">
                            
                            
                                <button type="submit" name="subscribe" id="mc-embedded-subscribe" class="btn btn-danger">Request a Demo!</button>
                            </div>
                        </div>
                    </form>
                </div>
                <p class="text-center copyright">Copyright &copy;  2014 <?php bloginfo('name'); ?> - All rights reserved</p>
            </div>
        </div>
    </div>
  </div>
</footer>

<?php wp_footer(); ?>
