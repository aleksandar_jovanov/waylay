<?php $catname = 'team'; ?>
<div class="wrap container" role="document">
    <div class="content row">
        <main class="main <?php echo waylay_main_class(); ?>" role="main">
                  <div class="row">
                      <div class="col-sm-12 main-content">
                          <h1 class="page-title"><?php echo waylay_title(); ?></h1>
                          <h4 class="page-desc"><?php $desc = get_post_meta( get_the_ID(), 'desc_title', true ); echo $desc; ?></h4>
                          <div class="team-wrap">
                            <?php $posts = get_posts("category_name=$catname&numberposts=6&offset=0");
                              foreach ($posts as $post) : start_wp();
                              $job_title = get_post_meta( get_the_ID(), 'team_title', true );
                              $linkedin = get_post_meta( get_the_ID(), 'team_linkedin', true );
//                              $skype = get_post_meta( get_the_ID(), 'team_skype', true );
                              $twitter = get_post_meta( get_the_ID(), 'team_twitter', true );
                              ?>
                              <div class="col-md-6 team-member">
                                    <div class="row">
                                        <div class="col-xs-6 member-lp">
                                            <div class="member-photo">
                                                <?php if (has_post_thumbnail( $post->ID ) ): ?>
                                                    <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'single-post-thumbnail'); ?>
                                                    <img src="<?php echo $image[0]; ?>" alt="<?php the_title_attribute(); ?>" class="img-circle">
                                                <?php endif; ?>
                                                <ul class="list-inline social-links">
                                                    <li><a target="_blank" href="<?php echo $linkedin; ?>" title="Linkedin" class="social-linkedin"></a></li>
                                                    <li><a target="_blank" href="<?php echo $twitter; ?>" title="Twitter" class="social-twitter"></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 member-about">
                                            <h5><?php the_title(); ?></h5>
                                            <p class="status"><?php echo $job_title; ?></p>
                                            <div class="about"><?php the_content(); ?></div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                          </div>
                      </div>
                  </div>
        </main><!-- /.main -->
    </div>
</div>