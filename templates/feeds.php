<div class="jumbotron feeds">
    <div class="container">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-6">
                    <div class="feed-box blog-news">
                        <h4>Blog news</h4>
                        <div class="blog-feeds">
                        <?php $catname = 'blog';
                            $posts = get_posts("category_name=$catname&numberposts=2&offset=0");
                            foreach ($posts as $post) : start_wp();
                        ?>
                        <div class="media">
                            <div class="pull-left red-box">
                                <span class="month"><?php echo get_the_time('M'); ?></span>
                                <span class="date"><?php echo get_the_time('d'); ?></span>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading"><?php the_title(); ?></h4>
                                <?php the_excerpt(); ?>
                              </div>
                        </div>
                        <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="feed-box twitter-feed">
                          <?php dynamic_sidebar('Footer-right'); ?>
                    </div>
                </div>
                <div class="clearfix"></div>
    
            </div>
        </div>
    </div>
</div>