<div class="jumbotron page-header-wrap blog-header-wrap">
    <div class="container">
        <div class="row">
            <h3 class="page-header-title"><?php $category = get_the_category(); echo $category[0]->cat_name; ?></h3>
            <div class="page-header-headline"><p>Brings smart reasoning to the<br/> <strong>Internet-of-Things</strong></p></div>
        </div>
    </div>
</div>
