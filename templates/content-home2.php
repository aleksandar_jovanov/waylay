<div class="smart">
    <div class="wrap container" role="document">
        <div class="content row">
            <div class="col-sm-12 text-center">
                <div class="row">
                    <div class="col-sm-3">
                        <a href="#smart-notif-section">
                            <div class="smart-icon smart-notification"></div>
                        </a>
                        <p>smart notification</p>
                    </div>
                    <div class="col-sm-3">
                        <a href="#smart-auto-section">
                            <div class="smart-icon smart-automation"></div>
                        </a>
                        <p>smart automation</p>
                    </div>
                    <div class="col-sm-3">
                        <a href="#smart-pred-section">
                            <div class="smart-icon smart-predictive"></div>
                        </a>
                        <p>smart predictive</p>
                        <p>maintenance</p>
                    </div>
                    <div class="col-sm-3">
                        <a href="#smart-inno-section">
                            <div class="smart-icon smart-inovation"></div>
                        </a>
                        <p>smart innovation</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php while (have_posts()) : the_post(); ?>
  <?php remove_filter( 'the_content', 'wpautop' ); the_content(); ?>
<?php endwhile; ?>
</div>




