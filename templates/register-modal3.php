<div class="modal fade" id="trialModal2" tabindex="-1" role="dialog" aria-labelledby="trialModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Free Trial</h4>
      </div>
        <div class="modal-body">
           <p>In just a few steps, you can complete the form for a <strong>30 days free trial</strong> of waylay.</p>
           <p>We will contact you shortly, to understand your needs, such that you can make most of the trial!</p>
           <?php echo do_shortcode( '[contact-form-7 id="5317" title="Send us message test"]' ) ?>
        </div>
      </div>
    </div>
  </div>