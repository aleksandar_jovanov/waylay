<?php get_template_part('templates/register', 'modal'); ?>
<footer class="footer">
  <div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="row">
                <div class="grey-box text-center req-demo">
                    <p>Let our technology speak for itself <button type="submit" class="btn btn-danger" data-toggle="modal" data-target="#registerModal">Request a Demo!</button></p>
                </div>
                <p class="text-center copyright">Copyright &copy;  2014 <?php bloginfo('name'); ?> - All rights reserved</p>
            </div>
        </div>
    </div>
  </div>
</footer>

<?php wp_footer(); ?>
