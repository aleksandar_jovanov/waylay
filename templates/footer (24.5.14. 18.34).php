<footer class="footer">
  <div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="row">
                <div class="grey-box">
                    <form  action="http://feedburner.google.com/fb/a/mailverify" method="post" target="popupwindow" onsubmit="window.open('http://feedburner.google.com/fb/a/mailverify?uri=waylayio', 'popupwindow', 'scrollbars=yes,width=550,height=520');return true">
                        <div class="form-group">
                            <div class="col-sm-9">
                                <label class="sr-only" for="notify-email">Email address</label>
                                <input type="text" name="email" class="form-control" id="notify-email" placeholder="Enter your email and get notified when our beta is launched ...">
                            </div>
                            <div class="col-sm-3">
                            <input type="hidden" value="waylayio" name="uri"/><input type="hidden" name="loc" value="en_US"/>
                                <button id="notify-btn" type="submit" class="btn btn-danger">Notify Me!</button>
                            </div>
                        </div>
                    </form>
                </div>
                <p class="text-center copyright">Copyright &copy;  2014 <?php bloginfo('name'); ?> - All rights reserved</p>
            </div>
        </div>
    </div>
  </div>
</footer>

<?php wp_footer(); ?>
