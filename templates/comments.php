<?php
  if (post_password_required()) {
    return;
  }

 if (have_comments()) : ?>
  <section id="comments">
    <h4 class="title"><?php _e('Comments', 'waylay'); ?></h4>

    <ol class="media-list">
      <?php wp_list_comments(array('walker' => new waylay_Walker_Comment)); ?>
    </ol>

    <?php if (get_comment_pages_count() > 1 && get_option('page_comments')) : ?>
    <nav>
      <ul class="pager">
        <?php if (get_previous_comments_link()) : ?>
          <li class="previous"><?php previous_comments_link(__('&larr; Older comments', 'waylay')); ?></li>
        <?php endif; ?>
        <?php if (get_next_comments_link()) : ?>
          <li class="next"><?php next_comments_link(__('Newer comments &rarr;', 'waylay')); ?></li>
        <?php endif; ?>
      </ul>
    </nav>
    <?php endif; ?>

    <?php if (!comments_open() && !is_page() && post_type_supports(get_post_type(), 'comments')) : ?>
    <div class="alert alert-warning">
      <?php _e('Comments are closed.', 'waylay'); ?>
    </div>
    <?php endif; ?>
  </section><!-- /#comments -->
<?php endif; ?>

<?php if (!have_comments() && !comments_open() && !is_page() && post_type_supports(get_post_type(), 'comments')) : ?>
  <section id="comments">
    <div class="alert alert-warning">
      <?php _e('Comments are closed.', 'waylay'); ?>
    </div>
  </section><!-- /#comments -->
<?php endif; ?>

<?php if (comments_open()) : ?>
  <section id="respond">
    <h4 class="title"><?php _e('Send us a message.', 'waylay'); ?></h4>
    <p class="cancel-comment-reply"><?php cancel_comment_reply_link(); ?></p>
    <?php if (get_option('comment_registration') && !is_user_logged_in()) : ?>
      <p><?php printf(__('You must be <a href="%s">logged in</a> to post a comment.', 'waylay'), wp_login_url(get_permalink())); ?></p>
    <?php else : ?>
      <form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">
        <?php if (is_user_logged_in()) : ?>
          <p>
            <?php printf(__('Logged in as <a href="%s/wp-admin/profile.php">%s</a>.', 'waylay'), get_option('siteurl'), $user_identity); ?>
            <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="<?php __('Log out of this account', 'waylay'); ?>"><?php _e('Log out &raquo;', 'waylay'); ?></a>
          </p>
        <?php else : ?>
          <div class="form-group">
            <div class="row">
                <div class="col-xs-6">
                  <input type="text" class="form-control" name="author" id="author" value="<?php echo esc_attr($comment_author); ?>" placeholder="<?php _e('Name', 'waylay'); if ($req) _e(' (required)', 'waylay'); ?>" size="22" <?php if ($req) echo 'aria-required="true"'; ?>>
                </div>
                <div class="col-xs-6">
                  <input type="email" class="form-control" name="email" id="email" value="<?php echo esc_attr($comment_author_email); ?>" placeholder="<?php _e('E-mail (will not be published)', 'waylay'); if ($req) _e(' (required)', 'waylay'); ?>" size="22" <?php if ($req) echo 'aria-required="true"'; ?>>
                </div>
              </div>  
          </div>
        <?php endif; ?>
        <div class="form-group">
          <textarea name="comment" id="comment" class="form-control" placeholder="<?php _e('Your Message', 'waylay'); ?>" rows="5" aria-required="true"></textarea>
        </div>
        <p><input name="submit" class="pull-right btn btn-warning" type="submit" id="submit" value="<?php _e('Send', 'waylay'); ?>"></p>
        <?php comment_id_fields(); ?>
        <?php do_action('comment_form', $post->ID); ?>
      </form>
    <?php endif; ?>
  </section><!-- /#respond -->
<?php endif; ?>
