<div class="jumbotron carousel-wrap">
    <div class="container">
        <div class="row text-right">
            <div class="bg">
                <img alt="Waylay" class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/base.png"/>
            </div>
            <div id="home-carousel" class="carousel slide carousel-fade" data-ride="carousel">
                <ol class="carousel-indicators">
                  <li data-target="#carousel" data-slide-to="0" class="active"></li>
                  <li data-target="#carousel" data-slide-to="1"></li>
                  <li data-target="#carousel" data-slide-to="2"></li>
                </ol>
                <!-- Carousel items -->
                <div class="carousel-inner">
                    <div class="active item slide1">
                        <img alt="Smart" class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/smart.png"/>
                        <div class="text-box">
                            <div class="inner">
                                <h1 class="headline">SMART REASONING FOR <br>the Internet-of-Things</h1>
                                <h2>Turning data into actionable decisions</h2>
                                <!--<button type="submit" class="btn btn-danger" data-toggle="modal" data-target="#registerModal">Request a Demo!</button><br class="visible-md-block visible-lg-block" />-->
                                <button type="submit" class="btn btn-danger btn2 trial-track" data-toggle="modal" data-target="#trialModal">Start a Free Trial</button><br class="visible-md-block visible-lg-block" />
                                <a href="features/" class="btn btn-transparent">Learn more <i class="fa fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="item slide2">
                        <img alt="Telco" class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/telco.png"/>
                        <div class="text-box">
                            <div class="inner">
                                <h1 class="headline">PROACTIVE CUSTOMER CARE</h1>
                                <h2>Rules engine for better customer experience management</h2>
                                <!--<button type="submit" class="btn btn-danger" data-toggle="modal" data-target="#registerModal">Request a Demo!</button><br class="visible-md-block visible-lg-block" />-->
                                <button type="submit" class="btn btn-danger btn2 trial-track" data-toggle="modal" data-target="#trialModal">Start a Free Trial</button><br class="visible-md-block visible-lg-block" />
                                <a href="features/" class="btn btn-transparent">Learn more <i class="fa fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="item slide3">
                        <img alt="Maintenance" class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/maintanance.png"/>
                        <div class="text-box">
                            <div class="inner">
                                <h1 class="headline">PREDICTIVE MAINTENANCE</h1>
                                <h2>Combine, anticipate, optimize</h2>
                                <!--<button type="submit" class="btn btn-danger" data-toggle="modal" data-target="#registerModal">Request a Demo!</button><br class="visible-md-block visible-lg-block" />-->
                                <button type="submit" class="btn btn-danger btn2 trial-track" data-toggle="modal" data-target="#trialModal">Start a Free Trial</button><br class="visible-md-block visible-lg-block" />
                                <a href="features/" class="btn btn-transparent">Learn more <i class="fa fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
          </div>
        </div>
    </div>
</div>
