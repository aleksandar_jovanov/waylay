<?php
require_once('geoplugin.class.php');
$geoplugin = new geoPlugin();
$geoplugin->locate();
$eu_code = $geoplugin->continentCode;
?>

<div class="pricing-table-wrap">
    <div class="wrap container" role="document">
        <div class="content">
            <main class="main" role="main">
                          <div class="pricing-tables attached">
                              <div class="row">
                                  <?php while (have_posts()) : the_post(); ?>
                                    <?php the_content(); ?>
                                  <?php endwhile; ?>
                                   <div class="col-sm-3 col-md-3">
                                 <div class="plan first green">
                                    <div class="head"><h2>Developer</h2><small><em>Perfect to explore and integrate</em></small></div>  
                                    <ul class="item-list">
                                         <li><i class="fa fa-user fa-lg"></i> <strong>1</strong> administrator</li>
                                         <li><i class="fa fa-tasks fa-lg"></i> <strong>10</strong> tasks</li>
                                         <li><i class="fa fa-sitemap fa-lg"></i> <strong>10</strong> nodes per task</li>
                                         <li><i class="fa fa-lightbulb-o fa-lg"></i> <strong>15K</strong> invocations</li>
                                         <li><i class="fa fa-envelope fa-lg"></i> best effort</li>
                                    </ul>
                                    <div class="price">
                                        <?php if ($eu_code == 'EU') { ?>
                                            <h3><span class="symbol">€</span>19</h3>
                                        <?php } else {?>
                                            <h3><span class="symbol">$</span>19</h3>
                                        <?php } ?>
                                      <h4>per month</h4>
                                    </div>
                                    <button type="button" class="btn trial-track" data-toggle="modal" data-target="#trialModal">Start Free Trial</button>
                                 </div>
                                </div>
                                <div class="col-sm-3 col-md-3 ">
                                    <div class="plan recommended yellow">
                                        <div class="head"><h2>Starter</h2><small><em>For trials and small roll-out</em></small></div>  
                                        <ul class="item-list">
                                             <li><i class="fa fa-users fa-lg"></i> <strong>2</strong> administrators</li>
                                             <li><i class="fa fa-tasks fa-lg"></i> <strong>300</strong> tasks</li>
                                             <li><i class="fa fa-sitemap fa-lg"></i> <strong>10</strong> nodes per task</li>
                                             <li><i class="fa fa-lightbulb-o fa-lg"></i> <strong>500K</strong> invocations</li>
                                             <li><i class="fa fa-envelope fa-lg"></i> e-mail support</li>
                                        </ul>
                                        <div class="price">
                                            <?php if ($eu_code == 'EU') { ?>
                                                <h3><span class="symbol">€</span>149</h3>
                                            <?php } else {?>
                                                <h3><span class="symbol">$</span>149</h3>
                                            <?php } ?>
                                          <h4>per month</h4>
                                        </div>
                                        <button type="button" class="btn trial-track" data-toggle="modal" data-target="#trialModal">Start Free Trial</button>
                                     </div>
                                </div>
                                <div class="col-sm-3 col-md-3 ">
                                    <div class="plan violet">
                                      <div class="head"><h2>Lite</h2><small><em>Deployments with small number of devices</em></small></div>  
                                      <ul class="item-list">
                                           <li><i class="fa fa-users fa-lg"></i> <strong>5</strong> administrators</li>
                                           <li><i class="fa fa-tasks fa-lg"></i> <strong>1500</strong> tasks</li>
                                           <li><i class="fa fa-sitemap fa-lg"></i> <strong>20</strong> nodes per task</li>
                                           <li><i class="fa fa-lightbulb-o fa-lg"></i> <strong>2M</strong> invocations</li>
                                           <li><i class="fa fa-envelope fa-lg"></i><i class="fa fa-phone fa-lg"></i> 4 hours</li>
                                      </ul>
                                      <div class="price">
                                            <?php if ($eu_code == 'EU') { ?>
                                                <h3><span class="symbol">€</span>499</h3>
                                            <?php } else {?>
                                                <h3><span class="symbol">$</span>499</h3>
                                            <?php } ?>
                                        <h4>per month</h4>
                                      </div>
                                      <button type="button" class="btn trial-track" data-toggle="modal" data-target="#trialModal">Start Free Trial</button>
                                 </div>
                                </div>
                                <div class="col-sm-3 col-md-3 ">
                                    <div class="plan last red">
                                      <div class="head"><h2>Professional</h2><small><em>Scaling-up</em></small></div>
                                      <ul class="item-list">
                                           <li><i class="fa fa-users fa-lg"></i> <strong>10</strong> administrators</li>
                                           <li><i class="fa fa-tasks fa-lg"></i> <strong>7500</strong> tasks</li>
                                           <li><i class="fa fa-sitemap fa-lg"></i> <strong>20</strong> nodes per task</li>
                                           <li><i class="fa fa-lightbulb-o fa-lg"></i> <strong>10M</strong> invocations</li>
                                           <li><i class="fa fa-envelope fa-lg"></i><i class="fa fa-phone fa-lg"></i> platinum</li>
                                      </ul>
                                      <div class="price">
                                            <?php if ($eu_code == 'EU') { ?>
                                                <h3><span class="symbol">€</span>1499</h3>
                                            <?php } else {?>
                                                <h3><span class="symbol">$</span>1499</h3>
                                            <?php } ?>
                                        <h4>per month</h4>
                                      </div>
                                      <button type="button" class="btn trial-track" data-toggle="modal" data-target="#trialModal">Start Free Trial</button>
                                 </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-sm-6 text-center">
                                    <ul class="list-unstyled inline-block">
                                      <li class="text-left"><i class="fa fa-info-circle"></i><small><em>the max number of custom sensors and actuators is 100 for any of the plans</em></small></li>
                                      <li class="text-left"><i class="fa fa-info-circle"></i><small><em>the max number of templates is 200 for any of the plans</em></small></li>
                                    </ul>
                                </div>
                                <div class="col-sm-6 text-center">
                                    <ul class="list-unstyled inline-block">
                                      <li class="text-left"><i class="fa fa-info-circle"></i><small><em>the max bucket size is 5 times the number of tasks in the plan</em></small></li>
                                      <li class="text-left"><i class="fa fa-info-circle"></i><small><em>waylay applies a bandwidth fair usage policy inline with our Terms of Use.</em></small></li>
                                    </ul>
                                </div>
                              </div>
                            </div>
            </main>
        </div>
    </div>
    <div class="pricing-premium">
        <div class="wrap container" role="document">
            <div class="content">
                <div class="pricing-tables attached">
                    
                    <div class="row">
                        <div class="col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1">
                          <div class="plan first last blue premium">
                            <div class="head"><h2>Premium</h2><small><em>Enterprise-size deployments</em></small></div>
                            <div class="row">
                              <div class="col-sm-4 col-md-4">
                                <ul class="item-list text-left">
                                     <li><i class="fa fa-line-chart fa-lg"></i> high volumes</li>
                                     <li><i class="fa fa-code-fork fa-lg"></i> custom integation</li>
                                     <li><i class="fa fa-life-ring fa-lg"></i> dedicated technical support</li>
                                </ul>
                              </div>
                              <div class="col-sm-4 col-md-4">
                                <ul class="item-list text-left">
                                     <li><i class="fa fa-pencil fa-lg"></i>advanced editor</li>
                                     <li><i class="fa fa-cubes fa-lg"></i>specific SLA</li>
                                     <li><i class="fa fa-eye-slash fa-lg"></i>private deployments</li>
                                </ul>
                              </div>
                              <div class="col-sm-4 col-md-4">
                                <div class="btn-wrap">
                                  <a class="btn" href="/contact-us/">Contact us</a>
                                </div>

                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
              </div>
          </div>
      </div>
  </div>
<div class="bottom-pricing">
    <div class="wrap container" role="document">
            <div class="content">
                <div class="col-sm-12">
                          <div class="row">
                            <div class="col-sm-12">
                                <!--<div class="page-header">-->
                                    <h2>Plan Terms</h2>
                                <!--</div>-->
                                <div class="row">  
                                    <div class="col-sm-6">
                                        <div class="text-block">
                                            <h3>What is a template?</h3>
                                            <p>A template is a generic piece of logic consisting of sensors, logical gates and actuators.</p>
                                        </div>
                                        <div class="text-block">
                                            <h3>What is a task?</h3>
                                            <p>A task is an instantiated template. When a task is started, it will invoke sensors, execute logic and invoke actuators.</p>
                                            <p>Waylay supports three types of tasks:</p>
                                            <ul>
                                                <li>periodic tasks that run at a fixed frequency.</li>
                                                <li>cron tasks that run according to a cron expression.</li>
                                                <li>onetime tasks that run only once.</li>
                                            </ul>
                                        </div>
                                        <div class="text-block">
                                            <h3>How is the number of nodes in a task counted?</h3>
                                            <p>The number of nodes in a task is the sum of the number of sensors, gates and actuators.</p>
                                        </div>
                                        <div class="text-block">
                                            <h3>How is the number of tasks counted?</h3>
                                            <p>Waylay counts the number of concurrently running tasks. Tasks that are stopped are not counted for. The number of tasks will in most cases be equal to the sum of the number of running cron tasks and the number of running periodic tasks. Keep in mind that onetime or debug tasks can temporarily increase the number of running tasks. Waylay allows you to only run one debug task at a time. At any point in time, the total number of tasks cannot exceed the limits of your subscription plan.</p>
                                        </div>
 					<div class="text-block">
                                            <h3>What is an invocation?</h3>
                                            <p>An invocation is logic executed at one particular point in time. Typically, one invocation includes the invocation of sensors, the evaluation of the logic and potentially the invocation of actuators.</p>
                                            <p>A couple of examples:</p>
                                            <ul>
                                                <li>A periodic task with a frequency of 15 minutes will result in 4x24x31 = 2976 invocations per month.</li>
                                                <li>A cron tasks that runs at 4am and 4pm every day, will result in 2x31 = 62 invocations per month.</li>
                                                <li>A one-time task will result in exactly 1 invocation.</li>
                                                <li>A debug task that runs for 3 minutes with a 10sec periodicity will result in 6x3 = 18 invocations.</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                       
 					<div class="text-block">
                                            <h3>What happens when I subscribe in the middle of the month?</h3>
                                            <p>You will be charged pro-rata for the running month and then you will be charged the full amount at the start of the next month.</p>                                     
                                        </div>
					<div class="text-block">
                                            <h3>When can I upgrade my plan?</h3>
                                            <p>Waylay allows you to upgrade at any point in time.  We will charge you prorate for the remainder of the month.</p>                                     
                                        </div><div class="text-block">
                                            <h3>When can I downgrade?</h3>
                                            <p>You can downgrade at any point in time. The downgrade will become effective as of the first day of the month subsequent to the downgrade.</p>                                     
                                        </div>
					<div class="text-block">
                                            <h3>What happens when I cancel?</h3>
                                            <p>You can cancel at any point in time. The service will effectively be cancelled the first day of the month subsequent to the month of cancellation.</p>                                     
                                        </div>
                                    </div>
                                </div>
                            </div>
                          </div>
                    </div>
                </div>
            </div>
        </div>
    </div>