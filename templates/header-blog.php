<div class="jumbotron page-header-wrap blog-header-wrap">
    <div class="container">
        <div class="row">
            <h3 class="page-header-title"><?php echo waylay_title(); ?></h3>
            <div class="page-header-headline"><p>Turning data into<br/> <strong>actionable decisions</strong></p></div>
        </div>
    </div>
</div>
