<div class="jumbotron page-header-wrap">
    <div class="container">
        <div class="row">
            <h3 class="page-header-title"><?php echo waylay_title(); ?></h3>
            <div class="page-header-headline"><p>Turning data into<br/> <strong>actionable decisions</strong></p></div>
        </div>
    </div>
</div>
<!--<div class="jumbotron jumbo-headline">
    <div class="container">
        <h1>waylay empowers developers with a flexible and scalable rules<br/>engine that connects devices, applications and systems</h1>
    </div>
</div>-->
