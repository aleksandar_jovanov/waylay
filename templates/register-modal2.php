<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Register form</h4>
      </div>
      <div class="modal-body">
           <form action="http://waylay.us8.list-manage.com/subscribe/post?u=c51ba39f3fb039e291480d0b8&amp;id=197048b176" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <div class="form-group">
                        <label class="sr-only" for="notify-email">Enter your email...</label>
                        <input type="email" value="" name="EMAIL" id="mce-EMAIL" class="form-control" placeholder="Enter your email...">  
                </div>
                <div class="form-group">
                    <div class="text-right">
                        <button id="notify-btn" type="submit" class="btn btn-danger">Submit <i class="fa fa-sign-out fa-lg"></i></button> 
                    </div>
                </div>
                    <div style="position: absolute; left: -5000px;"><input type="text" name="b_c51ba39f3fb039e291480d0b8_197048b176" tabindex="-1" value=""></div>
            </form>
            </div>
      </div>
    </div>
  </div>
</div>