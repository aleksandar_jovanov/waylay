<?php $catname = 'blog'; $desc = get_post_meta( get_the_ID(), 'desc_title', true );?>
<div class="wrap container" role="document">
    <div class="content row">
        <main class="main <?php echo waylay_main_class(); ?>" role="main">
                  <div class="row">
                      <div class="col-sm-12 main-content">
                          <h1 class="page-title"><?php echo waylay_title(); ?></h1>
                          <h4 class="page-desc"><?php echo $desc; ?></h4>
                          <div class="row">
                              <div class="col-sm-8">
                                  <div class="blog-wrap">
                                    <?php global $post;
                                        $blog = array('category' => 13,'posts_per_page'   => -1);
                                        $custom_posts = get_posts($blog);
                                        foreach($custom_posts as $post) : setup_postdata($post);
                                    ?>
                                    <!--Blog post start-->
                                    <div class="col-xs-12 blog-post">
                                        <div class="row">
                                            <div class="date-box">
                                                <div class="red-box">
                                                    <span class="month"><?php echo get_the_time('M'); ?></span>
                                                    <span class="date"><?php echo get_the_time('d'); ?></span>
                                                </div>
                                            </div>
                                            <div class="article">
                                                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                                <?php if (has_post_thumbnail( $post->ID ) ):
                                                    the_post_thumbnail('blog-thumb');
                                                 endif; ?>
                                                <?php get_template_part('templates/entry-meta'); ?>
                                                <div class="post-content">
                                                    <p><?php echo get_the_blog_excerpt(); ?> <a class="more" href="<?php the_permalink();?>">Read more</a></p>
                                                </div> 
                                            </div>    
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <!--Blog post end-->
                                    <?php endforeach; ?>
                                  </div>
                              </div>
                              <div class="col-sm-4">
                                <?php if (waylay_display_sidebar()) : ?>
                                  <div class="sidebar">
                                    <?php dynamic_sidebar('Blog'); ?>
                                <?php endif; ?>
                              </div>
                          </div>
                      </div>
                  </div>
            </div>
        </main><!-- /.main -->
    </div>
</div>