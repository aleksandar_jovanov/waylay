<header class="banner navbar navbar-default navbar-fixed-top" role="banner">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <?php
            /* Social links */
            if (has_nav_menu('social-top')) :
              wp_nav_menu(array('theme_location' => 'social-top', 'menu_class' => 'nav navbar-nav social-menu vissible-xs pull-right'));
            endif;
         ?> 
      <a class="navbar-brand" href="<?php echo home_url(); ?>/"><img alt="<?php bloginfo('name'); ?>" src="<?php echo get_template_directory_uri(); ?>/assets/img/waylay-logo.png" height="46"/></a>
    </div>
    
    <nav class="collapse navbar-collapse" role="navigation">
      <?php
        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav navbar-right'));
        endif;
      ?>
    </nav>
  </div>
</header>
