<?php if(is_page(4242)) {?>
<div class="jumbo-headline way-waylay">
    <div class="container">
        <h1>waylay is a pay-as-you-grow, cloud-native rules engine<br />for any OEM maker, integrator or vendor<br />of smart connected device solutions</h1>
    </div>
</div>
<?php } else {?>
<div class="jumbotron page-header-wrap">
    <div class="container">
        <div class="row">
            <h3 class="page-header-title"><?php echo waylay_title(); ?></h3>
            <div class="page-header-headline"><p>Turning data into<br/> <strong>actionable decisions</strong></p></div>
        </div>
    </div>
</div>
<?php } ?>
