<?php $catname = 'team'; ?>
<div class="wrap container" role="document">
    <div class="content row">
        <main class="main <?php echo waylay_main_class(); ?>" role="main">
                  <div class="row">
                      <div class="col-sm-12 main-content">
                          <h1 class="page-title"><?php echo waylay_title(); ?></h1>
                          <h4 class="page-desc"><?php $desc = get_post_meta( get_the_ID(), 'desc_title', true ); echo $desc; ?></h4>
                          <div class="team-wrap row">
                              <div class="col-sm-5">
                                  <?php 
                                    $firstInfo = false;
                                    $info=0;
                                  query_posts("category_name=$catname&numberposts=6&offset=0");
                                    if ( have_posts() ) : while ( have_posts() ) : the_post();
                                    $info++;
                                    $job_title = get_post_meta( get_the_ID(), 'team_title', true );
                                    $linkedin = get_post_meta( get_the_ID(), 'team_linkedin', true );
      //                              $skype = get_post_meta( get_the_ID(), 'team_skype', true );
                                    $twitter = get_post_meta( get_the_ID(), 'team_twitter', true );
                                    ?>
                                  <article id="info-<?php echo $info;?>" class="team-member-about <?php echo !$firstInfo ? "activeInfo":"";?>">
                                      <header>
                                          <div class="name"><?php the_title(); ?></div>
                                          <div class="social-icons">
                                              <ul class="list-inline">
                                                  <?php if ($twitter != ""){ ?>
                                                  <li class="tw">
                                                      <a target="_blank" href="<?php echo $twitter; ?>">
                                                          <span class="fa-stack">
                                                            <i class="fa fa-circle fa-stack-2x"></i>
                                                            <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                                                          </span>
                                                      </a>
                                                  </li>
                                                  <?php } 
                                                  if ($linkedin != ""){ ?>
                                                  <li class="in">
                                                      <a target="_blank" href="<?php echo $linkedin; ?>">
                                                          <span class="fa-stack">
                                                            <i class="fa fa-circle fa-stack-2x"></i>
                                                            <i class="fa fa-linkedin fa-stack-1x fa-inverse"></i>
                                                          </span>
                                                      </a>
                                                  </li>
                                                  <?php } ?>
                                              </ul>
                                          </div>
                                          <div class="clearfix"></div>
                                      </header>
                                      <h5><?php echo $job_title; ?></h5>
                                      <?php the_content(); ?>
                                  </article>
                                  <?php $firstInfo = true;endwhile;endif;?>
                              </div>
                              <div class="col-sm-7">
                                  <div class="team-images float-left">
                                      <ul class="list-inline">
                                          <?php
                                            $first = false;
                                            $i=0;
                                            query_posts("category_name=$catname&numberposts=6&offset=0");
                                            if ( have_posts() ) : while ( have_posts() ) : the_post();
                                            $i++;
                                            ?>
                                          <li><a data-info="<?php echo $i; ?>" href="javascript:void(0);" class="team-img-link <?php echo !$first ? "active":"";?>"><?php if (has_post_thumbnail( $post->ID ) ): ?>
                                                    <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'single-post-thumbnail'); ?>
                                                        <img class="img-circle img-responsive" src="<?php echo $image[0]; ?>" alt="<?php the_title_attribute(); ?>" width="160">
                                                    <?php endif; ?></a></li>  
                                         <?php $first = true;endwhile;endif; ?>
                                      </ul>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
        </main><!-- /.main -->
    </div>
</div>