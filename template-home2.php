<?php
/*
Template Name: Second home screen
*/
?>
<?php get_template_part('templates/header', 'home2'); ?>
<?php get_template_part('templates/content', 'home2'); ?>
<?php get_template_part('templates/register', 'modal'); ?>
<?php
    get_template_part('templates/partners');
    // Template footer
    get_template_part('templates/footer-home3');
?>
